from os import getcwd, listdir, mkdir
from os.path import join, exists

import spacy
from spacy import displacy
import pandas as pd


def read_txts():
    folder_path = join(getcwd(), "phrases")
    files = listdir(folder_path)
    phrases = {}
    for file in files:
        filename = file.replace(".txt", "")
        file_path = join(folder_path, file)
        with open(file_path, "r") as f:
            phrases[filename] = f.readlines()
    return phrases


def main():
    nlp = spacy.load("el_core_news_lg")
    output_path = join(getcwd(), "output")
    if not exists(output_path):
        mkdir(output_path)
    txts = read_txts()
    for txt, lines in txts.items():
        new_lines = []
        counter = 0
        cnt = 0
        if not exists(join(output_path, txt)):
            mkdir(join(output_path, txt))
        df = pd.DataFrame(columns=["Phrase", "Token", "Dependency", "Head Text", "Hed pos", "Children"])
        for line in lines:
            doc = nlp(line)
            svg = displacy.render(doc, style="dep")
            img_file = join(output_path, txt, "{}_{}.svg".format(txt, counter))
            with open(img_file, "w", encoding="utf-8") as f:
                f.write(svg)
            new_lines.append("====================================================================================\n")
            new_lines.append(line + "\n")
            new_lines.append("=====================================================================================\n")
            t = ["Token", "Dependency", "Head Text", "Hed pos", "Children", "\n"]
            new_lines.append("\t".join(t))
            for token in doc:
                children = " ".join([child.text for child in token.children])
                df.loc[cnt] = [line, token.text, token.dep_, token.head.text, token.head.pos_, children]
                tkn = "\t\t".join([token.text, token.dep_, token.head.text, token.head.pos_, children, "\n"])
                new_lines.append(tkn)
                cnt += 1
            counter += 1
        file_path = join(output_path, txt, txt + ".txt")
        csv_file_path = join(output_path, txt, "{}.csv".format(txt))
        df.to_csv(csv_file_path, sep=',', encoding='utf-8')
        with open(file_path, "w") as f:
            f.writelines(new_lines)


if __name__ == '__main__':
    main()
